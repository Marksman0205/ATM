package com.dayuanit.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.dayuanit.dao.impl.DetialDaoImpl;
import com.dayuanit.dao.impl.UserDaoImpl;
import com.dayuanit.domain.Detail;
import com.dayuanit.domain.User;
import com.dayuanit.service.UserService;
import com.dayuanit.util.DataBaseUtil;

public class UserServiceImpl implements UserService{
	
	private DetialDaoImpl detaildao = new DetialDaoImpl();
	private UserDaoImpl userdao = new UserDaoImpl();
	
	//TODO 开户
	public void openAccount(String username, String cardNumber, int amount){
		Connection conn = DataBaseUtil.getConn();
		//开启事物，设置为手动提交
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			//增加账户信息
			User user = new User();
			user.setUsername(username);
			user.setCardNumber(cardNumber);
			user.setBalance(amount);
			System.out.println(user);
			int id = userdao.addUser(user);
			if(0 == id) {
				System.out.println("openAccount is failed");
				conn.rollback();
				return;
			}
			
			user = userdao.queryUser(cardNumber);
			//增加流水账
			Detail detail = new Detail();
			detail.setUserId(user.getId());
			detail.setCardNumber(cardNumber);
			detail.setAmount(amount);
			detail.setDescMsg("开户预留金额");
			
			id = detaildao.addDetail(detail);
			if(0 ==id) {
				System.out.println("增加流水失败");
				conn.rollback();
				return;
			}
		
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}	
					
	}
	
	//TODO 存款
	public void deposit(String cardNumber, int amount) {
		
		if(amount <= 0) {
			System.out.println("输入金额不合法");
			return;
		}
		
		Connection conn = DataBaseUtil.getConn();
		try{
			conn.setAutoCommit(false);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		//验证账户是否存在，并增加金额
		User user = userdao.queryUser(cardNumber);
		if(null == user) {
			System.out.println("the card not access");
			return;
		}
		
	    try {
			int balance = user.getBalance();
			balance += amount;
			int rows = userdao.modifyBalance(balance, cardNumber);
			if(1 != rows) {
				System.out.println("add money is failed");
				conn.rollback();
				return;
			}
			
			//增加流水
			Detail detail = new Detail();
			detail.setUserId(user.getId());
			detail.setCardNumber(cardNumber);
			detail.setAmount(amount);
			detail.setDescMsg("存钱");
			
			int id = detaildao.addDetail(detail);
			if(0 == id) {
				System.out.println("add detail and deposit is failed");
				conn.rollback();
				return;
			}
		
			conn.commit();
	    } catch (SQLException e) {
				// TODO Auto-generated catch block
	    	try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    e.printStackTrace();
		}		
	}
	
	//TODO 取款
	public void draw(String cardNumber, int amount) {
		
		if(amount <= 0) {
			System.out.println("输入金额不合法");
			return;
		}
		
		//验证账户是否存在
		User user = userdao.queryUser(cardNumber);
		if(null == user) {
			System.out.println("the card not access");
			return;
		}
		
		Connection conn = DataBaseUtil.getConn();
		try{
			conn.setAutoCommit(false);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		//取出金额
	    try {
			int balance = user.getBalance();
			if(amount > balance) {
				System.out.println("余额不足");
			}
			balance -= amount;
			int rows = userdao.modifyBalance(balance, cardNumber);
			if(1 != rows) {
				System.out.println("add money is failed");
				conn.rollback();
				return;
			}
			
			//增加流水
			Detail detail = new Detail();
			detail.setUserId(user.getId());
			detail.setCardNumber(cardNumber);
			detail.setAmount(amount);
			detail.setDescMsg("取钱");
			
			int id = detaildao.addDetail(detail);
			if(0 == id) {
				System.out.println("add detail and deposit is failed");
				conn.rollback();
				return;
			}
		
			conn.commit();
	    } catch (SQLException e) {
				// TODO Auto-generated catch block
	    	try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    e.printStackTrace();
		}
	}
	
	//TODO 转账
	public void transfer(String inCardNumber, String outCardNumber, int amount) {
		
		if(amount <= 0) {
			System.out.println("输入金额不合法");
			return;
		}
		
		//验证账户是否存在
		User inuser = userdao.queryUser(inCardNumber);
	    if(null == inuser) {
			System.out.println("the card not access");
		    return;
		}
	    
	    Connection conn = DataBaseUtil.getConn();
		try{
			conn.setAutoCommit(false);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		//TODO 查询余额
		try {
			User outuser = userdao.queryUser(outCardNumber);
			int balance = outuser.getBalance();
			if(amount > balance) {
				System.out.println("余额不足");
			}
			//TODO 转出金额
			balance -= amount;
			int rows = userdao.modifyBalance(balance, outCardNumber);
			if(1 != rows) {
				System.out.println("sub money is failed");
				conn.rollback();
				return;
			}
			//TODO 增加金额
			balance = inuser.getBalance();
			balance += amount;
			rows = userdao.modifyBalance(balance, inCardNumber);
			if(1 != rows) {
				System.out.println("add money is failed");
				conn.rollback();
				return;
			}
			//TODO 增加流水
			//TODO 增加收款用户流水
			Detail indetail = new Detail();
			indetail.setUserId(inuser.getId());
			indetail.setCardNumber(inCardNumber);
			indetail.setAmount(amount);
			indetail.setDescMsg("收到转账");
			int id = detaildao.addDetail(indetail);
			if(0 ==id) {
				System.out.println("转账失败");
				conn.rollback();
				return;
			}
			//TODO 增加付款用户流水
			Detail outdetail = new Detail();
			outdetail.setUserId(inuser.getId());
			outdetail.setCardNumber(outCardNumber);
			outdetail.setAmount(amount);
			outdetail.setDescMsg("收到转账");
			id = detaildao.addDetail(outdetail);
			if(0 ==id) {
				System.out.println("转账失败");
				conn.rollback();
				return;
			}
			
			conn.commit();
		} catch (SQLException e) {
				// TODO Auto-generated catch block
	    	try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    e.printStackTrace();
		}
	}
	
	//TODO 查询余额
	public void queryBalance(String cardNumber) {
		
		//TODO 查询卡号是否正确
		User user = userdao.queryUser(cardNumber);
		if(null == user) {
			System.out.println("卡号不存在");
			return;
		}
		System.out.println("账户余额为：" + user.getBalance());
	}
	
	//TODO 查询流水账
	public void queryDetails(String cardNumber) {
		//TODO 查询卡号是否正确
		User user = userdao.queryUser(cardNumber);
		if(null == user) {
			System.out.println("卡号不存在");
			return;
		}
		
		//TODO 查询流水账
		List<Detail> list = detaildao.queryDetail(cardNumber);
		for(Detail detail : list) {		
			System.out.println(detail);
		}
	}


}
