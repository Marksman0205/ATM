package com.dayuanit.service;

public interface UserService {

	//TODO 开户
	void openAccount(String username, String cardNumber, int amount);
	
	//TODO 存款
	void deposit(String cardNumber, int amount);
	
	//TODO 取款
	void draw(String cardNumber, int amount);
	
	//TODO 转账
	void transfer(String inCardNumber, String outCardNumber, int amount);
	
	//TODO 查询余额
	void queryBalance(String cardNumber);
	
	//TODO 查询流水账
	void queryDetails(String cardNumber);
}
