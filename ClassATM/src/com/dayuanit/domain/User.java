package com.dayuanit.domain;

import java.util.Date;

public class User {
	private int id ;
	private String username;
	private String cardNumber;
	private int balance;
	private Date createTime;
	
	public User() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
    
	public String toString() {
		return "id=" + id + " username=" + username + " cardNumber=" + cardNumber + " balance=" + balance + " Date= " + createTime;
	}
}
