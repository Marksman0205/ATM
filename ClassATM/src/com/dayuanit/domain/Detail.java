package com.dayuanit.domain;

import java.util.Date;

public class Detail {
	private int id;
	private int userId;
	private String cardNumber;
	private int amount;
	private String descMsg;
	private Date createTime;
	
	public Detail() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getDescMsg() {
		return descMsg;
	}

	public void setDescMsg(String descMsg) {
		this.descMsg = descMsg;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String toString() {
		return "id= " + id + " user_id=" + userId + " amount=" + amount + " desc_msg=" + descMsg + " card_number=" + cardNumber;
	}
}
