package com.dayuanit.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DataBaseUtil {
	private static String url;
	private static String username;
	private static String psw;
	
	static Connection conn = null;	
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Properties ppt = new Properties();
			ppt.load(DataBaseUtil.class.getClassLoader().getResourceAsStream("jdbc.properties"));
			url = ppt.getProperty("jdbc.url");
			username = ppt.getProperty("jdbc.username");
			psw = ppt.getProperty("jdbc.psw");
			conn =  DriverManager.getConnection(url, username, psw);
			System.out.println("Connection is ok...");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Connection getConn() {
		return conn;
	}
	
}
