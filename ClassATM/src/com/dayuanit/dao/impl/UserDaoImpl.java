package com.dayuanit.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;//包含时间和日期，sql下面的Date是没有包含时间的

import com.dayuanit.dao.UserDao;
import com.dayuanit.domain.User;
import com.dayuanit.util.DataBaseUtil;

public class UserDaoImpl implements UserDao {
	
	//TODO增加数据库数据的方法insert
	public int addUser(User user) {
		String sql = "insert into user (username, card_number, balance, create_time) values (?, ?, ?, now());";
		Connection conn = null;
		System.out.println(sql);
		ResultSet rs = null;
		PreparedStatement  psm = null;
		int rows = 0;
		int id = 0;
		
		try {
			conn = DataBaseUtil.getConn();
			psm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			psm.setString(1, user.getUsername());
			psm.setString(2, user.getCardNumber());
			psm.setInt(3, user.getBalance());
			rows = psm.executeUpdate();//发送user 并返回执行成功的记录的条数
			if(1 != rows) {
				System.out.println("insert user id failed");
			}
			
			rs = psm.getGeneratedKeys();//获取增加的新值
			
			while(rs.next()) {
				id = rs.getInt(1);
				System.out.println("insert id is " + id);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try{
				if(null != psm) {
					psm.close();
				}
			} catch(Exception e1) {
				
			}
		}
		
		return id;
	}
	
	//TODO 修改数据库数据的方法update
	public int modifyBalance(int balance, String cardNumber) {
		String sql = "update user set balance = ? where card_number = ?;";
		System.out.println(sql);
		Connection conn = null;
		PreparedStatement psm = null;
		int rows = 0;
				
		try {
			conn = DataBaseUtil.getConn();
			psm = conn.prepareStatement(sql);//向数据库发送要执行的语句
			psm.setInt(1, balance);
			psm.setString(2, cardNumber);
			rows = psm.executeUpdate();
			if(1 != rows) {
				System.out.println("update id failed");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {		
				try {
					if(null != psm) {
						psm.close();
					}	
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		return rows;
		
	}
	
	//TODO 查询数据库数据的方法select
	public User queryUser(String cardNumber) {
		String sql = "select id, username, card_number, balance, create_time from user where card_number = ?;";
		System.out.println(sql);
		Connection conn = null;
		PreparedStatement psm = null;
		ResultSet rs = null;
		User user = null;
		
		try{
			conn = DataBaseUtil.getConn();
			psm = conn.prepareStatement(sql);
			psm.setString(1, cardNumber);
			rs = psm.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("id");
				String username = rs.getString("username");
				int balance = rs.getInt("balance");
				Timestamp tst = rs.getTimestamp("create_time");//getDate获取的是日期，getTime获取的时间，getTimestamp获取的时间加上日期
				user = new User();
				user.setId(id);
				user.setUsername(username);
				user.setBalance(balance);
				user.setCardNumber(cardNumber);
				user.setCreateTime(new Date(tst.getTime()));
				System.out.println(user);
				break;
			}
		} catch(SQLException e) {
			// TODOAuto-generated catch block
		} finally {
			try{
				if(null != psm) {
					psm.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return user;	
	}
}
