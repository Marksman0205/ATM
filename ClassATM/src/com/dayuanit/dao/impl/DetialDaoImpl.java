package com.dayuanit.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dayuanit.dao.DetialDao;
import com.dayuanit.domain.Detail;
import com.dayuanit.util.DataBaseUtil;

public class DetialDaoImpl implements DetialDao{

	public int addDetail(Detail detail) {
		String sql = "insert into detail(user_id, card_number, amount, desc_msg, create_time) values (?,?,?,?, now());";
		System.out.println(sql);
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement psm = null;
		int rows = 0;
		int id = 0;
		
		try {
			conn = DataBaseUtil.getConn();
			psm = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			psm.setInt(1, detail.getUserId());
			psm.setString(2, detail.getCardNumber());
			psm.setInt(3, detail.getAmount());
			psm.setString(4, detail.getDescMsg());
			rows = psm.executeUpdate();//发送sql，返回改变的行数
			if(1 != rows) {
				System.out.println("insert detail is failed");
			}
			
			rs = psm.getGeneratedKeys();
			while(rs.next()) {
				id = rs.getInt(1);
				System.out.println("insert detial is " + id);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try{
				if(null != psm) {
					psm.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
				
		
		return id;
		
	}
	
	public List<Detail> queryDetail(String cardNumber) {
		String sql = "select id, user_id,  card_number, amount, desc_msg, create_time from detail where card_number = ?;";
		System.out.println(sql);
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement psm = null;
		
		List<Detail> list = new ArrayList<Detail>();
	    Detail detail = null;
		
		try{
			conn = DataBaseUtil.getConn();
			psm = conn.prepareStatement(sql);
			psm.setString(1, cardNumber);
			rs = psm.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String cardNumber2 = rs.getString("card_number");
				int amount = rs.getInt("amount");
				String descMsg = rs.getString("desc_msg");
				Timestamp tt = rs.getTimestamp("create_time");
				
				detail = new Detail();
				detail.setId(id);
				detail.setUserId(userId);
				detail.setCardNumber(cardNumber2);
				detail.setAmount(amount);
				detail.setDescMsg(descMsg);
				detail.setCreateTime(new Date(tt.getTime()));
				list.add(detail);
			}
			
		} catch(Exception e ) {
			e.printStackTrace();
		} finally {
			try{
				if(null != psm) {
					psm.close();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return list;	
	}	
}
