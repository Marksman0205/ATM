package com.dayuanit.dao;

import com.dayuanit.domain.User;

public interface UserDao {

	int addUser(User user);
	
	int modifyBalance(int balance, String cardNumber);
	
	User queryUser(String cardNumber);
}
