package com.dayuanit.dao;

import java.util.List;

import com.dayuanit.domain.Detail;

public interface DetialDao {
	
	int addDetail(Detail detail);
	
	List<Detail> queryDetail(String cardNumber);
}
